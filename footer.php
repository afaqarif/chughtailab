<!-- Start Footer Section -->
<footer class="main-footer site-footer footer pt-3 pt-md-5" id="footer">
   <div class="container">
      <div class="row py-3 py-md-5">
         <div class="col-sm-4 col-md-2 mb-4 mb-md-0">
            <div class="align_left mb-4 mb-md-0">
               <figure class="figure">
                  <div class="single_image-wrapper "><img src="./assets/img/Chughtai-Lab-Logo.png"></div>
               </figure>
               <p class="mt-3 footer-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </p>
            </div>
         </div>
         <div class="col-sm-4 col-md-2 mb-4 mb-md-0">
            <h3 class="menu-title">About</h3>
            <ul class="menu-list footer-menu-1 pl-0">
               <li  class="menu-item"><a href="#">About</a></li>
               <li class="menu-item"><a href="#">Number</a></li>
               <li class="menu-item"><a href="#">Head Office Location</a></li>
            </ul>
         </div>
         <div class="col-sm-4 col-md-2 mb-4 mb-md-0">
            <h3 class="menu-title">Careers</h3>
            <ul class="menu-list footer-menu-2 pl-0">
               <li  class="menu-item"><a href="#">Current Openings</a></li>
               <li class="menu-item"><a href="#">FCPS Residency Programs</a></li>
               <li class="menu-item"><a href="#">MPhil Admissions</a></li>
               <li class="menu-item"><a href="#">BSc Mlt admissions</a></li>
            </ul>
         </div>
         <div class="col-sm-4 col-md-2 mb-4 mb-md-0">
            <h3 class="menu-title">Blogs & Media</h3>
            <ul class="menu-list footer-menu-3 pl-0">
               <li class="menu-item"><a href="#">Blogs</a></li>
               <li class="menu-item"><a href="#">Press Mentions</a></li>
               <li class="menu-item"><a href="#">Gallery</a></li>
               <li class="menu-item"><a href="#">Video</a></li>
               <li class="menu-item"><a href="#">Publications</a></li>
            </ul>
         </div>
         <div class=" col-sm-12 col-md-4 mb-4 mb-md-0">
            <h3 class="menu-title">Get Chughtai Lab Insider</h3>
            <form class="form-inline subscribe-form">
              <div class="form-group mt-2 mb-2">
                <label for="email" class="sr-only">Email</label>
                <input type="email" class="form-control rounded-0" id="email" placeholder="Enter Email">
              </div>
              <button type="submit" class="btn btn-primary mt-2 mb-2 rounded-0">Subscribes</button>
            </form>
            <ul class="social-icon pl-0 mt-2 d-flex">
               <li class="d-inline-block mx-1"><a href="#" target="_blank" class="d-inline-block text-center facebook-icon"><i class="fa fa-facebook"></i></a></li>
               <li class="d-inline-block mx-1"><a href="#" target="_blank" class="d-inline-block text-center twitter-icon"><i class="fa fa-twitter"></i></a></li>
               <li class="d-inline-block mx-1"><a href="#" target="_blank" class="d-inline-block text-center linkedin-icon"><i class="fa fa-linkedin"></i></a></li>
               <li class="d-inline-block mx-1"><a href="#" target="_blank" class="d-inline-block text-center instagram-icon"><i class="fa fa-instagram"></i></a></li>
               <li class="d-inline-block mx-1"><a href="#" target="_blank" class="d-inline-block text-center youtube-icon"><i class="fa fa-youtube"></i></a></li>
            </ul>
         </div>
      </div>
      <div class="footer-2 border-top pt-3">
         <div class="row">
            <div class="col-sm-12 col-md-5">
               <p class="copyright-footer text-black">All rights reserved by Chughtai Lab © Copyright 2020.</p>
            </div>
            <div class="col-sm-12 col-md-7 d-flex flex-column justify-content-center align-items-md-end align-items-start">
               <ul class="footer-2-menu pl-0 ">
                  <li class="menu-item d-block d-md-inline-block pr-3"><a href="#">Center Locator</a></li>
                  <li class="menu-item d-block d-md-inline-block pr-3"><a href="#">Subscribe to our Newsletter</a></li>
                  <li class="menu-item d-block d-md-inline-block pr-3"><a href="#">Contact Us</a></li>
                  <!-- <li class="menu-item d-block d-md-inline-block"><a href="#">- Social Media Platforms and App Link</a></li> -->
               </ul>
            </div>
         </div>
      </div>
   </div>
</footer>
<!-- End Footer Section -->
<!-- Start Scroll Button -->
<a class="js-go-to u-go-to-v1" href="#"
   data-type="fixed"
   data-position='{
   "bottom": 15,
   "right": 15
   }'
   data-offset-top="400"
   data-compensation="#js-header"
   data-show-effect="zoomIn">
   <i class="fa fa-angle-up" aria-hidden="true"></i>
</a>
<!--  End Scroll Button -->
</main>
<!-- End Main Section -->
<!-- JS Global Compulsory -->
<script src="./assets/js/libaray.js"></script>


<!-- <script src="./assets/js/jquery.min.js"></script>
<script src="./assets/js/jquery-migrate.min.js"></script>
<script src="./assets/js/popper.min.js"></script>
<script src="./assets/js/bootstrap.min.js"></script> -->
<!-- JS Implementing Plugins -->
<!-- <script src="./assets/js/appear.js"></script> -->

<!-- <script src="./assets/js/masterslider.min.js"></script>
<script src="./assets/js/jquery-ui.min.js"></script>
<script src="./assets/js/slick.js"></script> -->

<!-- <script src="./assets/js/jquery.cubeportfolio.min.js"></script> -->
<!-- <script src="./assets/js/gmaps.min.js"></script> -->
<!-- jQuery UI Core -->
<!-- <script src="./assets/js/widget.js"></script>
<script src="./assets/js/version.js"></script>
<script src="./assets/js/keycode.js"></script>
<script src="./assets/js/position.js"></script>
<script src="./assets/js/unique-id.js"></script>
<script src="./assets/js/safe-active-element.js"></script> -->


<!-- End jQuery UI Core -->
<!-- jQuery UI Helpers -->
<!-- <script src="./assets/js/menu.js"></script>
<script src="./assets/js//mouse.js"></script> -->
<!-- End jQuery UI Helpers -->
<!-- jQuery UI Widgets -->
<!-- <script src="./assets/js/datepicker.js"></script> -->
<!-- End jQuery UI Widgets -->
<!-- <script src="./assets/js/chosen.jquery.js"></script> -->
<!-- <script src="./assets/js/ImageSelect.jquery.js"></script> -->



<!-- JS Unify -->
<!-- <script src="./assets/js/hs.core.js"></script> -->
<!-- <script src="./assets/js/hs.hamburgers.js"></script>
<script src="./assets/js/hs.rating.js"></script>
<script src="./assets/js/hs.carousel.js"></script> -->
<!-- <script src="./assets/js/hs.header.js"></script> -->

<!-- <script src="./assets/js/hs.scroll-nav.js"></script> -->

<!-- <script src="./assets/js/hs.datepicker.js"></script> -->
<!-- <script src="./assets/js/hs.select.js"></script> -->

<!-- <script src="./assets/js/hs.cubeportfolio.js"></script> -->
<!-- <script src="./assets/js/hs.map.js"></script> -->
<!-- <script src="./assets/js/hs.go-to.js"></script> -->
<!-- <script src="./assets/js/jquery.magnific-popup.min.js"></script> -->
<!-- JS Customization -->
<script src="./assets/js/custom.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtt1z99GtrHZt_IcnK-wryNsQ30A112J0&callback=initMap" async></script> -->
</body>
</html>