<!-- Start Case Study Section -->
<section class="case-study-wrapper py-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="case-content-rap d-flex flex-column flex-md-row justify-content-around align-items-start align-items-md-center">
					<div class="case-heading d-flex flex-column flex-md-row align-items-start align-items-md-center mb-4 mb-md-0">
						<div class="case-heading-content pl-0 pl-md-4">
							<h2 class="text-white mb-0">Apki Sehat ka number!</h2>
							<span class="text-white"><a href="tel:03111456789" class="text-white"> 03-111 456 789.</a></span>
						</div>
					</div>
					<div class="case-btn mb-4 mb-md-0">
						<button type="button" class="text-uppercase">Call or Whatsapp</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Case Study Section -->