<section class="medical-sec">
	<div class="container-fluid">
		<div class="row no-gutters">
			<div class="col-sm-6">
				<div class="medical-lists">
					<div class="medical-item">
						<a href="#">
							<div class="center-logo">
								<img src="./assets/img/center-logo.jpg" alt="Medical Center">
							</div>
	                        <div class="center-desc">
	                        	<h4>Chughtai Medical Center Jail Rd</h4>
	                        	<p>10 Jail Rd, Main Gulberg Lahore</p>
	                        	<div class="center-info">
	                        		<div class="center-contact">
	                        			<p> <i class="fa fa-phone"></i> 123 456 987</p>
	                        		</div>
	                        	</div>
	                        </div>
                    	</a>
					</div>
					<div class="medical-item">
						<a href="#">
							<div class="center-logo">
								<img src="./assets/img/center-logo.jpg" alt="Medical Center">
							</div>
	                        <div class="center-desc">
	                        	<h4>Chughtai Medical Center Jail Rd</h4>
	                        	<p>10 Jail Rd, Main Gulberg Lahore</p>
	                        	<div class="center-info">
	                        		<div class="center-contact">
	                        			<p> <i class="fa fa-phone"></i> 123 456 987</p>
	                        		</div>
	                        	</div>
	                        </div>
                    	</a>
					</div>
					<div class="medical-item">
						<a href="#">
							<div class="center-logo">
								<img src="./assets/img/center-logo.jpg" alt="Medical Center">
							</div>
	                        <div class="center-desc">
	                        	<h4>Chughtai Medical Center Jail Rd</h4>
	                        	<p>10 Jail Rd, Main Gulberg Lahore</p>
	                        	<div class="center-info">
	                        		<div class="center-contact">
	                        			<p> <i class="fa fa-phone"></i> 123 456 987</p>
	                        		</div>
	                        	</div>
	                        </div>
                    	</a>
					</div>
					<div class="medical-item">
						<a href="#">
							<div class="center-logo">
								<img src="./assets/img/center-logo.jpg" alt="Medical Center">
							</div>
	                        <div class="center-desc">
	                        	<h4>Chughtai Medical Center Jail Rd</h4>
	                        	<p>10 Jail Rd, Main Gulberg Lahore</p>
	                        	<div class="center-info">
	                        		<div class="center-contact">
	                        			<p> <i class="fa fa-phone"></i> 123 456 987</p>
	                        		</div>
	                        	</div>
	                        </div>
                    	</a>
					</div>
					<div class="medical-item">
						<a href="#">
							<div class="center-logo">
								<img src="./assets/img/center-logo.jpg" alt="Medical Center">
							</div>
	                        <div class="center-desc">
	                        	<h4>Chughtai Medical Center Jail Rd</h4>
	                        	<p>10 Jail Rd, Main Gulberg Lahore</p>
	                        	<div class="center-info">
	                        		<div class="center-contact">
	                        			<p> <i class="fa fa-phone"></i> 123 456 987</p>
	                        		</div>
	                        	</div>
	                        </div>
                    	</a>
					</div>
					<div class="medical-item">
						<a href="#">
							<div class="center-logo">
								<img src="./assets/img/center-logo.jpg" alt="Medical Center">
							</div>
	                        <div class="center-desc">
	                        	<h4>Chughtai Medical Center Jail Rd</h4>
	                        	<p>10 Jail Rd, Main Gulberg Lahore</p>
	                        	<div class="center-info">
	                        		<div class="center-contact">
	                        			<p> <i class="fa fa-phone"></i> 123 456 987</p>
	                        		</div>
	                        	</div>
	                        </div>
                    	</a>
					</div>
					<div class="medical-item">
						<a href="#">
							<div class="center-logo">
								<img src="./assets/img/center-logo.jpg" alt="Medical Center">
							</div>
	                        <div class="center-desc">
	                        	<h4>Chughtai Medical Center Jail Rd</h4>
	                        	<p>10 Jail Rd, Main Gulberg Lahore</p>
	                        	<div class="center-info">
	                        		<div class="center-contact">
	                        			<p> <i class="fa fa-phone"></i> 123 456 987</p>
	                        		</div>
	                        	</div>
	                        </div>
                    	</a>
					</div>
					<div class="medical-item">
						<a href="#">
							<div class="center-logo">
								<img src="./assets/img/center-logo.jpg" alt="Medical Center">
							</div>
	                        <div class="center-desc">
	                        	<h4>Chughtai Medical Center Jail Rd</h4>
	                        	<p>10 Jail Rd, Main Gulberg Lahore</p>
	                        	<div class="center-info">
	                        		<div class="center-contact">
	                        			<p> <i class="fa fa-phone"></i> 123 456 987</p>
	                        		</div>
	                        	</div>
	                        </div>
                    	</a>
					</div>

				</div>
			</div>
			<div class="col-sm-6">
				<div class="medical-center-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d435521.408038387!2d74.05418885106005!3d31.482635206169014!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39190483e58107d9%3A0xc23abe6ccc7e2462!2sLahore%2C%20Punjab%2C%20Pakistan!5e0!3m2!1sen!2s!4v1605875577327!5m2!1sen!2s" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				</div>
			</div>		
		</div>
	</div>
	
</section>