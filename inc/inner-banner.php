<section class="inner-banner">
	<div class="inner-banner-img-wrapper">
		<figure class="inner-banner-img" style="background-image: url('./assets/img/inner-banner.jpg');">
		</figure>
	</div>
	<div class="inner-banner-titlebar">
		<div class="container ">
			<div class="row">
				<div class="col-md-12">
					<h1>About</h1>
				</div>
			</div>
		</div>
	</div>
</section>