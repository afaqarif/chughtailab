<!-- Start Search Section -->
<section class="g-bg2colors-primary-black-v1">
   <div class="container">
      <div class="row">
         <div class="col-md-3 g-flex-centered g-theme-bg-black-v1 g-py-50">
            <h2 class="text-uppercase g-line-height-1_2 g-font-weight-700 g-font-size-26 g-font-secondary g-color-white mb-0">
               <em class="d-block g-font-style-normal g-font-size-11 g-color-primary g-mb-5">Book information</em>
               Tour's search
            </h2>
         </div>
         <div class="col-md-9 g-bg-primary g-py-50">
            <form class="g-px-15">
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group g-font-secondary g-color-white g-mb-20 g-mb-0--md">
                        <label class="text-uppercase g-font-weight-700 g-font-size-11 g-mb-10">Destination</label>
                        <div class="input-group g-brd-primary--focus">
                           <select class="js-custom-select form-control h-100 u-form-control u-select-v1 g-color-white g-font-size-15 g-brd-none g-bg-primary-dark-v1 rounded-0 g-py-10 g-px-15" style="width: 100%;"
                              data-placeholder="Reservation date"
                              data-open-icon="fa fa-caret-down"
                              data-close-icon="fa fa-caret-up">
                              <option></option>
                              <option value="Department1">Department1</option>
                              <option value="Department2">Department2</option>
                              <option value="Department3">Department3</option>
                              <option value="Department4">Department4</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group g-font-secondary g-color-white g-mb-20 g-mb-0--md">
                        <label class="text-uppercase g-font-weight-700 g-font-size-11 g-mb-10">Date</label>
                        <div class="input-group g-brd-primary--focus">
                           <input class="js-datepicker form-control h-100 u-form-control u-datepicker-v1 g-color-white g-font-size-15 g-placeholder-inherit g-brd-none g-bg-primary-dark-v1 rounded-0 pr-0" type="text" placeholder="DD/MM/YY">
                           <div class="input-group-addon rounded-0 d-flex align-items-center g-color-white g-placeholder-inherit g-bg-primary-dark-v1 g-brd-none">
                              <i class="fa fa-calendar"></i>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group g-font-secondary g-color-white g-mb-20 g-mb-0--md">
                        <label class="text-uppercase g-font-weight-700 g-font-size-11 g-mb-10">Persons</label>
                        <div class="input-group">
                           <input class="form-control h-100 u-form-control g-color-white g-font-size-15 g-placeholder-inherit g-brd-none g-bg-primary-dark-v1 rounded-0 pr-0" type="text" placeholder="Number of persons">
                           <div class="input-group-addon rounded-0 d-flex align-items-center g-color-white g-placeholder-inherit g-bg-primary-dark-v1 g-brd-none">
                              <i class="fa fa-users"></i>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group g-font-secondary g-color-white g-mb-20 g-mb-0--md">
                        <label class="d-block text-uppercase g-hidden-xs-down g-font-weight-700 g-font-size-11 g-mb-10">&nbsp;</label>
                        <button class="btn btn-md text-uppercase u-btn-theme-black-v1 g-font-weight-700 g-font-size-12 rounded-0 g-py-13 g-px-25" type="submit" role="button">Search</button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</section>
<!-- End Search Section -->