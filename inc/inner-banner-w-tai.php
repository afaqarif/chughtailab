<section class="inner-banner">
	<div class="inner-banner-img-wrapper">
		<figure class="inner-banner-img" style="background-image: url('./assets/img/inner-banner.jpg');">
		</figure>
	</div>
	<div class="inner-banner-titlebar">
		<div class="container ">
			<div class="row">
				<div class="col-md-12">
					<h1>Medical Center</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="shortcuts-bages">
	   <ul>
	      <li>
	         <a href="#" class="u-btn-primary g-transition-0_3">
	            <span class="d-none d-md-block">Book Free Home Sample Collection</span>
	            <span class="d-block d-md-none"><i class="fa fa-ambulance" aria-hidden="true"></i></span>
	         </a>
	      </li>
	      <li>
	         <a href="#" class="u-btn-primary g-transition-0_3">
	            <span class="d-none d-md-block">Online Test Results</span>
	            <span class="d-block d-md-none"><i class="fa fa-files-o" aria-hidden="true"></i></span>
	         </a>
	      </li>
	   </ul>
	</div>
</section>