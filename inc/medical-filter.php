<section class="g-bg2colors-primary-black-v1">
    <div class="container">
        <div class="row">
            <div class="col-md-3 offset-md-2 g-flex-centered g-theme-bg-black-v1 g-py-25">
                <h2 class="text-uppercase g-line-height-1_2 g-font-weight-700 g-font-size-26 g-font-secondary g-color-white mb-0">
                    Our Location</h2>
            </div>
            <div class="col-md-6 g-bg-primary g-py-25">
                <form class="g-px-15">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group g-font-secondary g-color-white g-mb-20 g-mb-0--md">
                                <div class="input-group g-brd-primary--focus">
                                    <select class="js-custom-select form-control h-100 u-form-control u-select-v1 g-color-white g-font-size-15 g-brd-none g-bg-primary-dark-v1 rounded-0 g-py-10 g-px-15">
                                        <option value="date">Selct City</option>
                                        <option value="Department1">Lahore</option>
                                        <option value="Department2">Karachi</option>
                                        <option value="Department3">Islamabad</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group g-font-secondary g-color-white g-mb-20 g-mb-0--md">
                                <button class="btn btn-md text-uppercase u-btn-theme-black-v1 g-font-weight-700 g-font-size-12 rounded-0 g-py-13 g-px-25" type="submit" role="button">Filter</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>