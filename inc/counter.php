<section id="counter-section" class="py-5">
<div class="container">
  <div class="row text-center">
    <div class="col-6 col-md-3 coun-border-right mb-5 mb-md-0">
      <a href="#" class="counter mb-4 mb-md-0">
       <img src="./assets/img/counter/Events-Worldwide.png">
        <h2 class="count-title timer" data-to="5000" data-speed="100"></h2>
        <p class="count-text ">STARTUPS</p>
      </a>
    </div>
    <div class="col-6 col-md-3 coun-border-right mb-5 mb-md-0">
      <a href="#" class="counter mb-4 mb-md-0">
        <img src="./assets/img/counter/Workers-Employed.png">
        <h2 class="count-title timer" data-to="10000" data-speed="1700"></h2>
        <p class="count-text ">ATTENDEES</p>
      </a>
    </div>
  <hr>
    <div class="col-6 col-md-3 coun-border-right mb-5 mb-md-0">
      <a href="#" class="counter mb-4 mb-md-0">
        <img src="./assets/img/counter/members.png">
        <h2 class="count-title timer" data-to="2000" data-speed="100"></h2>
        <p class="count-text ">MEMBERS</p>
      </a>
    </div>
      <div class="col-6 col-md-3 mb-5 mb-md-0">
        <a href="#" class="counter mb-4 mb-md-0">
          <img src="./assets/img/counter/Chapters.png">
          <h2 class="count-title timer" data-to="61" data-speed="1"></h2>
          <p class="count-text ">CHAPTERS</p>
        </a>
      </div>
    </div>
  </div>
  </section>