<!-- Start Parallax Video Section -->
<section class="parallax wow fadeIn f-desk-img fl-mob-img video-section" data-stellar-background-ratio="0.1" style="visibility: visible; padding-top: 115px;padding-bottom: 115px;">
    <div class="opacity-extra-medium"></div>
    <div class="container position-relative">
        <div class="row">
            <div class="col-lg- col-md-6 text-center center-col">
                <a type="button" data-toggle="modal" data-target="#exampleModalCenter">
                    <img src="./assets/img/icon-play.png" class="width-30" alt="dangslab-icon-play" title="icon-play" data-no-retina="" width="238" height="238">
                </a>
                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header border-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <iframe  width="853" height="480"src="https://www.youtube.com/embed/89t5u2E2l88" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--  End Parallax Video Section -->