<!-- Start Our Value Section -->
<section class="our-value-sec p-5">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <h2 class="text-center bl-clr text-uppercase pb-5 font-weight-bold">Our <span>Value</span></h2>
         </div>
         <div class="col-12 col-md-6 val-item mb-5">
            <div class="icon d-flex flex-row align-content-center mb-3">
               <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" xml:space="preserve" width="32" height="32">
                  <defs xmlns="http://www.w3.org/2000/svg">
                     <linearGradient gradientUnits="userSpaceOnUse" id="grad493380" x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset="0%" stop-color="rgb(36, 147, 224)"></stop>
                        <stop offset="100%" stop-color="rgb(36, 147, 224)"></stop>
                     </linearGradient>
                  </defs>
                  <g class="nc-icon-wrapper" fill="#444444">
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="29" y1="18" x2="3" y2="18" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="3" y1="6" x2="29" y2="6" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 10,27 10,31 5,31 5,27 " stroke-linejoin="miter"></polyline>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 27,27 27,31 22,31 22,27 " stroke-linejoin="miter"></polyline>
                     <path fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M29,27H3V4 c0-1.657,1.343-3,3-3h20c1.657,0,3,1.343,3,3V27z" stroke-linejoin="miter"></path>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="7.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="24.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                  </g>
               </svg>
               <h3 class="bl-clr text-uppercase font-weight-bold pl-4">TRANSPORTATION</h3>
            </div>
            <div class="contents">
               <p class="text-dark">Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches.</p>
            </div>
         </div>
         <div class="col-12 col-md-6 val-item mb-5">
            <div class="icon d-flex flex-row align-content-center mb-3">
               <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" xml:space="preserve" width="32" height="32">
                  <defs xmlns="http://www.w3.org/2000/svg">
                     <linearGradient gradientUnits="userSpaceOnUse" id="grad493380" x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset="0%" stop-color="rgb(36, 147, 224)"></stop>
                        <stop offset="100%" stop-color="rgb(36, 147, 224)"></stop>
                     </linearGradient>
                  </defs>
                  <g class="nc-icon-wrapper" fill="#444444">
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="29" y1="18" x2="3" y2="18" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="3" y1="6" x2="29" y2="6" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 10,27 10,31 5,31 5,27 " stroke-linejoin="miter"></polyline>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 27,27 27,31 22,31 22,27 " stroke-linejoin="miter"></polyline>
                     <path fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M29,27H3V4 c0-1.657,1.343-3,3-3h20c1.657,0,3,1.343,3,3V27z" stroke-linejoin="miter"></path>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="7.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="24.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                  </g>
               </svg>
               <h3 class="bl-clr text-uppercase font-weight-bold pl-4">TRANSPORTATION</h3>
            </div>
            <div class="contents">
               <p class="text-dark">Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches.</p>
            </div>
         </div>
         <div class="col-12 col-md-6 val-item mb-5">
            <div class="icon d-flex flex-row align-content-center mb-3">
               <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" xml:space="preserve" width="32" height="32">
                  <defs xmlns="http://www.w3.org/2000/svg">
                     <linearGradient gradientUnits="userSpaceOnUse" id="grad493380" x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset="0%" stop-color="rgb(36, 147, 224)"></stop>
                        <stop offset="100%" stop-color="rgb(36, 147, 224)"></stop>
                     </linearGradient>
                  </defs>
                  <g class="nc-icon-wrapper" fill="#444444">
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="29" y1="18" x2="3" y2="18" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="3" y1="6" x2="29" y2="6" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 10,27 10,31 5,31 5,27 " stroke-linejoin="miter"></polyline>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 27,27 27,31 22,31 22,27 " stroke-linejoin="miter"></polyline>
                     <path fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M29,27H3V4 c0-1.657,1.343-3,3-3h20c1.657,0,3,1.343,3,3V27z" stroke-linejoin="miter"></path>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="7.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="24.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                  </g>
               </svg>
               <h3 class="bl-clr text-uppercase font-weight-bold pl-4">TRANSPORTATION</h3>
            </div>
            <div class="contents">
               <p class="text-dark">Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches.</p>
            </div>
         </div>
         <div class="col-12 col-md-6 val-item mb-5">
            <div class="icon d-flex flex-row align-content-center mb-3">
               <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" xml:space="preserve" width="32" height="32">
                  <defs xmlns="http://www.w3.org/2000/svg">
                     <linearGradient gradientUnits="userSpaceOnUse" id="grad493380" x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset="0%" stop-color="rgb(36, 147, 224)"></stop>
                        <stop offset="100%" stop-color="rgb(36, 147, 224)"></stop>
                     </linearGradient>
                  </defs>
                  <g class="nc-icon-wrapper" fill="#444444">
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="29" y1="18" x2="3" y2="18" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="3" y1="6" x2="29" y2="6" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 10,27 10,31 5,31 5,27 " stroke-linejoin="miter"></polyline>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 27,27 27,31 22,31 22,27 " stroke-linejoin="miter"></polyline>
                     <path fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M29,27H3V4 c0-1.657,1.343-3,3-3h20c1.657,0,3,1.343,3,3V27z" stroke-linejoin="miter"></path>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="7.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="24.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                  </g>
               </svg>
               <h3 class="bl-clr text-uppercase font-weight-bold pl-4">TRANSPORTATION</h3>
            </div>
            <div class="contents">
               <p class="text-dark">Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches.</p>
            </div>
         </div>
         <div class="col-12 col-md-6 val-item">
            <div class="icon d-flex flex-row align-content-center mb-3">
               <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" xml:space="preserve" width="32" height="32">
                  <defs xmlns="http://www.w3.org/2000/svg">
                     <linearGradient gradientUnits="userSpaceOnUse" id="grad493380" x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset="0%" stop-color="rgb(36, 147, 224)"></stop>
                        <stop offset="100%" stop-color="rgb(36, 147, 224)"></stop>
                     </linearGradient>
                  </defs>
                  <g class="nc-icon-wrapper" fill="#444444">
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="29" y1="18" x2="3" y2="18" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="3" y1="6" x2="29" y2="6" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 10,27 10,31 5,31 5,27 " stroke-linejoin="miter"></polyline>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 27,27 27,31 22,31 22,27 " stroke-linejoin="miter"></polyline>
                     <path fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M29,27H3V4 c0-1.657,1.343-3,3-3h20c1.657,0,3,1.343,3,3V27z" stroke-linejoin="miter"></path>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="7.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="24.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                  </g>
               </svg>
               <h3 class="bl-clr text-uppercase font-weight-bold pl-4">TRANSPORTATION</h3>
            </div>
            <div class="contents">
               <p class="text-dark">Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches.</p>
            </div>
         </div>
         <div class="col-12 col-md-6 val-item">
            <div class="icon d-flex flex-row align-content-center mb-3">
               <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" xml:space="preserve" width="32" height="32">
                  <defs xmlns="http://www.w3.org/2000/svg">
                     <linearGradient gradientUnits="userSpaceOnUse" id="grad493380" x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset="0%" stop-color="rgb(36, 147, 224)"></stop>
                        <stop offset="100%" stop-color="rgb(36, 147, 224)"></stop>
                     </linearGradient>
                  </defs>
                  <g class="nc-icon-wrapper" fill="#444444">
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="29" y1="18" x2="3" y2="18" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <line data-cap="butt" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-miterlimit="10" x1="3" y1="6" x2="29" y2="6" stroke-linecap="butt" stroke-linejoin="miter"></line>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 10,27 10,31 5,31 5,27 " stroke-linejoin="miter"></polyline>
                     <polyline data-color="color-2" fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" points=" 27,27 27,31 22,31 22,27 " stroke-linejoin="miter"></polyline>
                     <path fill="none" stroke="url(#grad493380)" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M29,27H3V4 c0-1.657,1.343-3,3-3h20c1.657,0,3,1.343,3,3V27z" stroke-linejoin="miter"></path>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="7.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                     <circle data-color="color-2" data-stroke="none" fill="none" cx="24.5" cy="22.5" r="1.5" stroke="url(#grad493380)"></circle>
                  </g>
               </svg>
               <h3 class="bl-clr text-uppercase font-weight-bold pl-4">TRANSPORTATION</h3>
            </div>
            <div class="contents">
               <p class="text-dark">Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches.</p>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- End Our Value Section -->