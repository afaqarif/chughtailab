<!-- Start About Video Section -->
<section class="content-with-image py-5 ">
	<div class="container">
		<div class="row py-0 py-md-5">
			<div class="col-12 col-md-5 mb-4 mb-md-0 d-flex justify-content-center flex-column">
				<h2 class="text-uppercase bl-clr font-weight-bold">About <span>One</span></h2>
				<span class="subheading"> <strong> Quality tours crafted by local experts,</strong></span>
				<div class="divder my-3 my-md-5"></div>
				<div class="cwi-description">
					<p class="text-dark">
						We have a selection of over 50 tour programs that range from introductory multi-country itineraries to more regional in-depth options.
					</p>
					<p class="text-dark pt-2 pt-md-4">
						We have a selection of over 50 tour programs that range from introductory multi-country itineraries to more regional in-depth options.
					</p>
				</div>
				<a href="#" class="cwi-link pt-2 pt-md-4  text-uppercase bl-clr font-weight-bold" >Learn More</a>
			</div>
			<div class="col-12 col-md-6 offset-md-1">
				<figure class="cwi-img">
					<img src="./assets/img/video-min.jpg" alt="" >
				</figure>
				<p class="text-center text-dark pt-5">Lorem ipsum dolor sit amet, consectetur</p>
			</div>
		</div>
	</div>
</section>

<!-- End About Video Section -->