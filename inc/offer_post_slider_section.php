<!-- Start Post Slider -->
<section id="ourTours" class="g-theme-bg-gray-light-v1 g-py-50">
   <div class="container g-max-width-780 text-center">
      <div class="u-heading-v8-2 g-mb-40">
         <h2 class="text-uppercase u-heading-v8__title g-font-weight-700 g-font-size-40 mb-0">
            <strong class="h6 d-inline-block g-theme-bg-black-v1 g-color-white g-mb-20">Our Services</strong>
            <br><span class="g-color-primary">We can</span> offer
         </h2>
      </div>
   </div>
   <!-- Product Blocks -->
   <div class="js-carousel g-px-25"
      data-infinite="true"
      data-slides-show="5"
      data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-width-45 g-height-45 g-font-size-default g-color-white g-bg-primary g-theme-bg-black-v1--hover"
      data-arrow-left-classes="fa fa-chevron-left g-left-0"
      data-arrow-right-classes="fa fa-chevron-right g-right-0"
      data-responsive='[{
      "breakpoint": 1200,
      "settings": {
      "slidesToShow": 5
      }
      }, {
      "breakpoint": 992,
      "settings": {
      "slidesToShow": 3
      }
      }, {
      "breakpoint": 768,
      "settings": {
      "slidesToShow": 2
      }
      }, {
      "breakpoint": 576,
      "settings": {
      "slidesToShow": 1
      }
      }]'>
      <div class="js-slide g-px-7">
         <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <div class="js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
                     data-rating="2.5"
                     data-spacing="5"></div>
                  <p class="">dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
      </div>
      <div class="js-slide g-px-7">
         <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <div class="js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
                     data-rating="2.5"
                     data-spacing="5"></div>
                  <p class="">dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
      </div>
      <div class="js-slide g-px-7">
         <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <div class="js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
                     data-rating="2.5"
                     data-spacing="5"></div>
                  <p class="">dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
      </div>
      <div class="js-slide g-px-7">
         <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <div class="js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
                     data-rating="2.5"
                     data-spacing="5"></div>
                  <p class="">dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
      </div>
      <div class="js-slide g-px-7">
         <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <div class="js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
                     data-rating="2.5"
                     data-spacing="5"></div>
                  <p class="">dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
      </div>
      <div class="js-slide g-px-7">
         <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <div class="js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
                     data-rating="2.5"
                     data-spacing="5"></div>
                  <p class="">dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
      </div>
      <div class="js-slide g-px-7">
         <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <div class="js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
                     data-rating="2.5"
                     data-spacing="5"></div>
                  <p class="">dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
      </div>
      <div class="js-slide g-px-7">
         <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <div class="js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
                     data-rating="2.5"
                     data-spacing="5"></div>
                  <p class="">dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
      </div>
   </div>
   <!-- Product Blocks -->
</section>
<!-- End Post Slider -->