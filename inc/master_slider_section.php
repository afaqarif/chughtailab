<!-- Start Master Slider Section -->
<div class="slider-wrapper g-pos-rel">
<section class="master-slider" id="masterslider">
   <div class="ms-slide">
      <img src="./assets/img/blank.gif" alt="Image description"
         data-src="./assets/img/banner/slide.jpg">
      <h3 class="ms-layer text-uppercase g-pos-rel g-line-height-1 g-font-weight-700 g-font-size-45 g-font-size-75--lg g-font-secondary g-color-white g-mb-10">Karlovy Vary, Czech</h3>
      <div class="ms-layer g-pos-rel g-line-height-1_2 g-max-width-550">
         <p class="g-mb-20 g-font-size-default g-font-size-18--md g-color-white-opacity-0_8">Donec erat urna, tincidunt at leo non, blandit finibus ante. Nunc venenatis risus in finibus dapibus. Ut ac massa sodales, mattis enim id, efficitur tortor.</p>
         <a class="btn btn-md text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 rounded-0 g-py-10 g-px-25" href="#">Take a tour</a>
      </div>
      <img class="ms-thumb" src="./assets/img/banner/thumb/slide.jpg" alt="Image description">
   </div>
   <div class="ms-slide">
      <img src="./assets/img/blank.gif" alt="Image description"
         data-src="./assets/img/banner/slide.jpg">
      <h3 class="ms-layer text-uppercase g-pos-rel g-line-height-1 g-font-weight-700 g-font-size-45 g-font-size-75--lg g-font-secondary g-color-white g-mb-10">Karlovy Vary, Czech</h3>
      <div class="ms-layer g-pos-rel g-line-height-1_2 g-max-width-550">
         <p class="g-mb-20 g-font-size-default g-font-size-18--md g-color-white-opacity-0_8">Donec erat urna, tincidunt at leo non, blandit finibus ante. Nunc venenatis risus in finibus dapibus. Ut ac massa sodales, mattis enim id, efficitur tortor.</p>
         <a class="btn btn-md text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 rounded-0 g-py-10 g-px-25" href="#">Take a tour</a>
      </div>
      <img class="ms-thumb" src="./assets/img/banner/thumb/slide.jpg" alt="Image description">
   </div>
   <div class="ms-slide">
      <img src="./assets/img/blank.gif" alt="Image description"
         data-src="./assets/img/banner/slide.jpg">
      <h3 class="ms-layer text-uppercase g-pos-rel g-line-height-1 g-font-weight-700 g-font-size-45 g-font-size-75--lg g-font-secondary g-color-white g-mb-10">Karlovy Vary, Czech</h3>
      <div class="ms-layer g-pos-rel g-line-height-1_2 g-max-width-550">
         <p class="g-mb-20 g-font-size-default g-font-size-18--md g-color-white-opacity-0_8">Donec erat urna, tincidunt at leo non, blandit finibus ante. Nunc venenatis risus in finibus dapibus. Ut ac massa sodales, mattis enim id, efficitur tortor.</p>
         <a class="btn btn-md text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 rounded-0 g-py-10 g-px-25" href="#">Take a tour</a>
      </div>
      <img class="ms-thumb" src="./assets/img/banner/thumb/slide.jpg" alt="Image description">
   </div>
   <div class="ms-slide">
      <img src="./assets/img/blank.gif" alt="Image description"
         data-src="./assets/img/banner/slide.jpg">
      <h3 class="ms-layer text-uppercase g-pos-rel g-line-height-1 g-font-weight-700 g-font-size-45 g-font-size-75--lg g-font-secondary g-color-white g-mb-10">Karlovy Vary, Czech</h3>
      <div class="ms-layer g-pos-rel g-line-height-1_2 g-max-width-550">
         <p class="g-mb-20 g-font-size-default g-font-size-18--md g-color-white-opacity-0_8">Donec erat urna, tincidunt at leo non, blandit finibus ante. Nunc venenatis risus in finibus dapibus. Ut ac massa sodales, mattis enim id, efficitur tortor.</p>
         <a class="btn btn-md text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 rounded-0 g-py-10 g-px-25" href="#">Take a tour</a>
      </div>
      <img class="ms-thumb" src="./assets/img/banner/thumb/slide.jpg" alt="Image description">
   </div>
   <div class="ms-slide">
      <img src="./assets/img/blank.gif" alt="Image description"
         data-src="./assets/img/banner/slide.jpg">
      <h3 class="ms-layer text-uppercase g-pos-rel g-line-height-1 g-font-weight-700 g-font-size-45 g-font-size-75--lg g-font-secondary g-color-white g-mb-10">Karlovy Vary, Czech</h3>
      <div class="ms-layer g-pos-rel g-line-height-1_2 g-max-width-550">
         <p class="g-mb-20 g-font-size-default g-font-size-18--md g-color-white-opacity-0_8">Donec erat urna, tincidunt at leo non, blandit finibus ante. Nunc venenatis risus in finibus dapibus. Ut ac massa sodales, mattis enim id, efficitur tortor.</p>
         <a class="btn btn-md text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 rounded-0 g-py-10 g-px-25" href="#">Take a tour</a>
      </div>
      <img class="ms-thumb" src="./assets/img/banner/thumb/slide.jpg" alt="Image description">
   </div>
   <div class="ms-slide">
      <img src="./assets/img/blank.gif" alt="Image description"
         data-src="./assets/img/banner/slide.jpg">
      <h3 class="ms-layer text-uppercase g-pos-rel g-line-height-1 g-font-weight-700 g-font-size-45 g-font-size-75--lg g-font-secondary g-color-white g-mb-10">Karlovy Vary, Czech</h3>
      <div class="ms-layer g-pos-rel g-line-height-1_2 g-max-width-550">
         <p class="g-mb-20 g-font-size-default g-font-size-18--md g-color-white-opacity-0_8">Donec erat urna, tincidunt at leo non, blandit finibus ante. Nunc venenatis risus in finibus dapibus. Ut ac massa sodales, mattis enim id, efficitur tortor.</p>
         <a class="btn btn-md text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 rounded-0 g-py-10 g-px-25" href="#">Take a tour</a>
      </div>
      <img class="ms-thumb" src="./assets/img/banner/thumb/slide.jpg" alt="Image description">
   </div>
   <div class="ms-slide">
      <img src="./assets/img/blank.gif" alt="Image description"
         data-src="./assets/img/banner/slide.jpg">
      <h3 class="ms-layer text-uppercase g-pos-rel g-line-height-1 g-font-weight-700 g-font-size-45 g-font-size-75--lg g-font-secondary g-color-white g-mb-10">Karlovy Vary, Czech</h3>
      <div class="ms-layer g-pos-rel g-line-height-1_2 g-max-width-550">
         <p class="g-mb-20 g-font-size-default g-font-size-18--md g-color-white-opacity-0_8">Donec erat urna, tincidunt at leo non, blandit finibus ante. Nunc venenatis risus in finibus dapibus. Ut ac massa sodales, mattis enim id, efficitur tortor.</p>
         <a class="btn btn-md text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 rounded-0 g-py-10 g-px-25" href="#">Take a tour</a>
      </div>
      <img class="ms-thumb" src="./assets/img/banner/thumb/slide.jpg" alt="Image description">
   </div>
   
</section>
<div class="shortcuts-bages">
   <ul>
      <li>
         <a href="#" class="u-btn-primary g-transition-0_3">
            <span class="d-none d-md-block">Book Free Home Sample Collection</span>
            <span class="d-block d-md-none"><i class="fa fa-ambulance" aria-hidden="true"></i></span>
         </a>
      </li>
      <li>
         <a href="#" class="u-btn-primary g-transition-0_3">
            <span class="d-none d-md-block">Online Test Results</span>
            <span class="d-block d-md-none"><i class="fa fa-files-o" aria-hidden="true"></i></span>
         </a>
      </li>
   </ul>
</div>
</div>
<!-- End Master Slider Section -->