<!-- Start Subscribe Section-->
<section id="subscribe" class="u-bg-overlay g-bg-black-opacity-0_7--after g-bg-img-hero g-py-150--md g-py-50 " style="background-image: url(./assets/img/img-temp/1400x534/img1.jpg);">
   <div class="container u-bg-overlay__inner text-center g-color-white">
      <div class="row justify-content-center">
         <div class="col-md-8">
            <h2 class="text-uppercase g-font-weight-700 g-font-size-56 g-font-secondary g-color-white g-mb-40">
               <!-- <span class="g-color-primary">Get 10%</span> -->Get 10% discount
            </h2>
            <p class="g-color-white-opacity-0_8 g-mb-65">Donec eleifend mauris eu leo varius consectetur. Aliquam luctus a lorem ac rutrum. Cras in nulla id mi ornare vestibulum. Donec et magna nulla. Pellentesque ut ipsum id nibh pretium blandit quis ac erat.</p>
            <form role="form">
               <div class="row">
                  <div class="col-md-6 form-group g-mb-35">
                     <input class="form-control form-control-lg g-placeholder-white g-font-size-15 g-color-white g-bg-transparent g-bg-transparent--focus g-brd-white g-brd-white--focus rounded-0 g-pa-10" name="email" type="text" placeholder="Your name">
                  </div>
                  <div class="col-md-6 form-group g-mb-35">
                     <input class="form-control form-control-lg g-placeholder-white g-font-size-15 g-color-white g-bg-transparent g-bg-transparent--focus g-brd-white g-brd-white--focus rounded-0 g-pa-10" name="email" type="email" placeholder="Your email">
                  </div>
               </div>
               <button class="btn btn-md text-uppercase u-btn-primary g-font-weight-700 g-font-size-12 rounded-0 g-py-10 g-px-25" type="submit">Send message</button>
            </form>
         </div>
      </div>
   </div>
</section>
<!-- Start Subscribe Section-->