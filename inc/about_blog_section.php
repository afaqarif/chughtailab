<!-- Start About Blog Section -->

<section class="about-blg-section py-5">
   <div class="container">
      <div class="row">
         <div class="col-md-12 ">
         <div class="d-flex flex-column flex-md-row justify-content-between align-items-center pb-5 pt-3">
            <h2 class="text-uppercase mb-0 bl-clr font-weight-bold">FROM OUR TRAVEL BLOG</h2>
            <a href="#" class="bl-clr text-uppercase font-weight-bold">SEE ALL BLOG</a>
         </div>
      </div>
         <div class="col-md-4">
            <div class="ab-ar-rap">
                <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <p class="">dapibus quis sapien id phar etra iaculis est 
                  dapibus quis sapien id phar etra iaculis est 
               dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
            </div>
         </div>
         <div class="col-md-4">
            <div class="ab-ar-rap">
                <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <p class="">dapibus quis sapien id phar etra iaculis est 
                  dapibus quis sapien id phar etra iaculis est 
               dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
            </div>
         </div>
         <div class="col-md-4">
            <div class="ab-ar-rap">
                <a class="d-block g-pos-rel g-text-underline--none--hover g-parent g-bg-white g-bg-primary--hover g-transition-0_3 g-color-white--hover" href="#">
            <div class="u-bg-overlay g-bg-black-opacity-0_3--after">
               <img class="img-fluid" src="./assets/img/img-temp/600x600/img1.jpg" alt="Image description">
            </div>
            <div class="g-pa-20">
               <div class="g-mb-20">
                  <h3 class="text-uppercase g-font-weight-700 g-font-size-default g-font-secondary g-mb-5 g-color-white--parent-hover">Crete</h3>
                  <p class="">dapibus quis sapien id phar etra iaculis est 
                  dapibus quis sapien id phar etra iaculis est 
               dapibus quis sapien id phar etra iaculis est</p>
               </div>
               <strong class="d-inline-block g-line-height-1 g-font-weight-700 g-font-size-11 g-bg-primary g-bg-white--parent-hover g-color-primary--parent-hover g-color-white g g-pa-10" href="#">Read more</strong>
            </div>
         </a>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- End About Blog Section -->