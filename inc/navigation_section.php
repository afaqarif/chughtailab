<!-- Start Navigation Section -->
<header id="js-header" class="u-header u-header--sticky-top u-header--change-appearance">
<div class="u-header__section">
      <nav class="navbar navbar-expand-lg g-py-0">
         <div class="container g-pos-rel">
            <!-- Logo -->
            <a href="#" class="js-go-to navbar-brand u-header__logo"
               data-type="static">
               <img class="logo-img" src="./assets/img/Chughtai-Lab-Logo.png" alt="Image description">
               <img src="./assets/img/white-logo.png" class="white-logo" alt="Image description">
            </a>
            <!-- End Logo -->
            <!-- Navigation -->
            <div class="collapse navbar-collapse align-items-center flex-sm-row" id="navBar" data-mobile-scroll-hide="true">
               <ul id="js-scroll-nav" class="navbar-nav text-uppercase g-font-weight-700 g-font-size-11 g-pt-20 g-pt-0--lg ml-auto">
                  <li class="nav-item g-mr-15--lg g-mb-7 g-mb-0--lg active">
                     <a href="#" class="nav-link p-0">About</a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                     <a href="#" class="nav-link p-0">For Patients</a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                     <a href="#" class="nav-link p-0">Services</a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                     <a href="#" class="nav-link p-0">International Passengers</a>
                  </li>
                  <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                     <a href="#" class="nav-link p-0">Contact us</a>
                  </li>
               </ul>
            </div>
            <!-- End Navigation -->
            <!-- Responsive Toggle Button -->
            <button class="navbar-toggler btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-20 g-right-0" type="button"
               aria-label="Toggle navigation"
               aria-expanded="false"
               aria-controls="navBar"
               data-toggle="collapse"
               data-target="#navBar">
            <span class="hamburger hamburger--slider">
            <span class="hamburger-box">
            <span class="hamburger-inner"></span>
            </span>
            </span>
            </button>
            <!-- End Responsive Toggle Button -->
         </div>
      </nav>
   </div>
</header>
<!-- End Navigation Section -->