<!-- Start Customer Reviews Section -->
<section class="ct-re-section bl-bg py-5">
	<div class="container">
		<div class="row py-0 py-md-5">
			<div class="col-md-12">
				<h2 class="text-white text-white font-weight-bold mb-0"> CUSTOMER REVIEWS</h2>
				<div class="full-divider my-3 my-md-5"></div>
			</div>
			<div class="col-sm-12 col-md-3 d-none d-md-block">
				<div class="card customer-rev-box border-0 py-3 px-3">
					<div class="card-body">
						<h3 class="card-title text-white font-weight-bold">Excellent</h3>
						<div class="ab-rev-star py-2 js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
							data-rating="2.5"
						data-spacing="5"></div>
						<p class="card-text text-white py-3">Based on 702 reviews See all reviews here</p>
						<img src="./assets/img/card-icon.png">
					</div>
				</div>
			</div>
			<div class="col-sm-12 offset-md-1 col-md-8">
				<div  class="js-carousel u-carousel-v5"
					data-infinite="true"
					data-slides-scroll="true"
					data-slides-show="2"
					data-arrows-classes="u-arrow-v1 g-pos-abs g-top-100 g-width-45 g-height-45 g-font-size-default g-color-white g-bg-primary g-theme-bg-black-v1--hover"
					data-arrow-left-classes="fa fa-chevron-left g-left-0"
					data-arrow-right-classes="fa fa-chevron-right g-right-0"
					data-responsive='[{
					"breakpoint": 1200,
					"settings": {
					"slidesToShow": 2
					}
					}, {
					"breakpoint": 768,
					"settings": {
					"slidesToShow": 1
					}
					}]'>
					
					<div class="js-slide">
						<div class="rev-box-rap">
							<div class="top-con d-flex justify-content-between pb-3">
								<div class="slide-rev js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
									data-rating="2.5"
								data-spacing="5"></div>
								<span class="rev-time text-wihte text-white">3 hours ago</span>
							</div>
							<h5 class="text-white">Great tour director and driver!!!!</h5>
							<p class="text-white">We just got back from our best vacation ever and we wanted to thank you and your team for your excellent work…</p>
							<strong class="text-white">Dale Briggs</strong>
						</div>
					</div>
					<div class="js-slide">
						<div class="rev-box-rap">
							<div class="top-con d-flex justify-content-between pb-3">
								<div class="slide-rev js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
									data-rating="2.5"
								data-spacing="5"></div>
								<span class="rev-time text-wihte text-white">3 hours ago</span>
							</div>
							<h5 class="text-white">Great tour director and driver!!!!</h5>
							<p class="text-white">We just got back from our best vacation ever and we wanted to thank you and your team for your excellent work…</p>
							<strong class="text-white">Dale Briggs</strong>
						</div>
					</div>
					<div class="js-slide">
						<div class="rev-box-rap">
							<div class="top-con d-flex justify-content-between pb-3">
								<div class="slide-rev js-rating info-v5-6__rating g-transition-0_3 g-color-primary g-color-white--parent-hover g-font-size-default g-mb-10"
									data-rating="2.5"
								data-spacing="5"></div>
								<span class="rev-time text-wihte text-white">3 hours ago</span>
							</div>
							<h5 class="text-white">Great tour director and driver!!!!</h5>
							<p class="text-white">We just got back from our best vacation ever and we wanted to thank you and your team for your excellent work…</p>
							<strong class="text-white">Dale Briggs</strong>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Customer Reviews Section -->