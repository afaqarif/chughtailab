<!-- Start Testimonials Section  -->
<section id="testimonials" class="g-py-50">
   <div class="container g-max-width-780 text-center g-mb-70">
      <div class="u-heading-v8-2 g-mb-40">
         <h2 class="text-uppercase u-heading-v8__title g-font-weight-700 g-font-size-40 mb-0">
            <strong class="h6 d-inline-block g-theme-bg-black-v1 g-color-white g-mb-20">Testimonials</strong>
            <br><span class="g-color-primary">Customers</span> say's
         </h2>
      </div>
      <p class="mb-0">Vestibulum at turpis enim. Aliquam dapibus quis sapien id pharetra. Vivamus iaculis est vitae libero tempus, in sollicitudin est consectetur porttitor iaculis pretium</p>
   </div>
   <div class="container-fluid px-0">
      <div id="carouselCus2" class="js-carousel js-carousel_single-item-thumbs5--v3__thumbs u-carousel-v1 g-parent g-mb-50"
         data-infinite="true"
         data-center-mode="true"
         data-variable-width="true"
         data-slides-show="5"
         data-arrows-classes="u-arrow-v1 g-absolute-centered g-width-35 g-height-40 g-font-size-26 g-color-gray g-bg-white opacity-0 g-opacity-1--parent-hover"
         data-arrow-left-classes="fa fa-angle-left g-ml-minus-142"
         data-arrow-right-classes="fa fa-angle-right g-ml-142"
         data-nav-for="#carouselCus3">
         <div class="js-thumb">
            <img src="./assets/img/img-temp/240x240/img1.jpg" alt="Image description">
         </div>
         <div class="js-thumb">
            <img src="./assets/img/img-temp/240x240/img2.jpg" alt="Image description">
         </div>
         <div class="js-thumb">
            <img src="./assets/img/img-temp/240x240/img3.jpg" alt="Image description">
         </div>
         <div class="js-thumb">
            <img src="./assets/img/img-temp/240x240/img4.jpg" alt="Image description">
         </div>
         <div class="js-thumb">
            <img src="./assets/img/img-temp/240x240/img5.jpg" alt="Image description">
         </div>
         <div class="js-thumb">
            <img src="./assets/img/img-temp/240x240/img6.jpg" alt="Image description">
         </div>
         <div class="js-thumb">
            <img src="./assets/img/img-temp/240x240/img7.jpg" alt="Image description">
         </div>
         <div class="js-thumb">
            <img src="./assets/img/img-temp/240x240/img8.jpg" alt="Image description">
         </div>
         <div class="js-thumb">
            <img src="./assets/img/img-temp/240x240/img9.jpg" alt="Image description">
         </div>
      </div>
      <div class="row justify-content-center no-gutters">
         <div id="carouselCus3" class="js-carousel text-center col-sm-8"
            data-infinite="true"
            data-fade="true"
            data-animation="linear"
            data-nav-for="#carouselCus2">
            <div class="js-slide">
               <h4 class="text-uppercase g-font-weight-700 g-font-size-default g-ma-10">Julia B. Mcraflane</h4>
               <em class="text-uppercase d-block g-font-style-normal g-font-weight-600 g-font-size-10 g-color-primary g-mb-15">Company CEO</em>
               <blockquote class="g-font-style-italic g-font-size-16 g-mb-0">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</blockquote>
            </div>
            <div class="js-slide">
               <h4 class="text-uppercase g-font-weight-700 g-font-size-default g-ma-10">Amy Clayton</h4>
               <em class="text-uppercase d-block g-font-style-normal g-font-weight-600 g-font-size-10 g-color-primary g-mb-15">Programmer</em>
               <blockquote class="g-font-style-italic g-font-size-16 g-mb-0">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</blockquote>
            </div>
            <div class="js-slide">
               <h4 class="text-uppercase g-font-weight-700 g-font-size-default g-ma-10">Fred Penner</h4>
               <em class="text-uppercase d-block g-font-style-normal g-font-weight-600 g-font-size-10 g-color-primary g-mb-15">Web Developer</em>
               <blockquote class="g-font-style-italic g-font-size-16 g-mb-0">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</blockquote>
            </div>
            <div class="js-slide">
               <h4 class="text-uppercase g-font-weight-700 g-font-size-default g-ma-10">Martina Saiz</h4>
               <em class="text-uppercase d-block g-font-style-normal g-font-weight-600 g-font-size-10 g-color-primary g-mb-15">UI Designer</em>
               <blockquote class="g-font-style-italic g-font-size-16 g-mb-0">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</blockquote>
            </div>
            <div class="js-slide">
               <h4 class="text-uppercase g-font-weight-700 g-font-size-default g-ma-10">Joseph B. Seward</h4>
               <em class="text-uppercase d-block g-font-style-normal g-font-weight-600 g-font-size-10 g-color-primary g-mb-15">Developer</em>
               <blockquote class="g-font-style-italic g-font-size-16 g-mb-0">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</blockquote>
            </div>
            <div class="js-slide">
               <h4 class="text-uppercase g-font-weight-700 g-font-size-default g-ma-10">Jane Lopex</h4>
               <em class="text-uppercase d-block g-font-style-normal g-font-weight-600 g-font-size-10 g-color-primary g-mb-15">Company CEO</em>
               <blockquote class="g-font-style-italic g-font-size-16 g-mb-0">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</blockquote>
            </div>
            <div class="js-slide">
               <h4 class="text-uppercase g-font-weight-700 g-font-size-default g-ma-10">James Ridgway</h4>
               <em class="text-uppercase d-block g-font-style-normal g-font-weight-600 g-font-size-10 g-color-primary g-mb-15">IT Manager</em>
               <blockquote class="g-font-style-italic g-font-size-16 g-mb-0">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</blockquote>
            </div>
            <div class="js-slide">
               <h4 class="text-uppercase g-font-weight-700 g-font-size-default g-ma-10">Patricia Burns</h4>
               <em class="text-uppercase d-block g-font-style-normal g-font-weight-600 g-font-size-10 g-color-primary g-mb-15">System Admin</em>
               <blockquote class="g-font-style-italic g-font-size-16 g-mb-0">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</blockquote>
            </div>
            <div class="js-slide">
               <h4 class="text-uppercase g-font-weight-700 g-font-size-default g-ma-10">Terry Thompson</h4>
               <em class="text-uppercase d-block g-font-style-normal g-font-weight-600 g-font-size-10 g-color-primary g-mb-15">Reviewer</em>
               <blockquote class="g-font-style-italic g-font-size-16 g-mb-0">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</blockquote>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- End Testimonials Section  -->