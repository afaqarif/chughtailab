<!-- Start Purposes Section -->
<section id="purposes" class="g-py-50">
   <div class="container g-max-width-780 text-center g-mb-70">
      <div class="u-heading-v8-2 g-mb-40">
         <h2 class="text-uppercase u-heading-v8__title g-font-weight-700 g-font-size-40 mb-0">
            <strong class="h6 d-inline-block g-theme-bg-black-v1 g-color-white g-mb-20">Special purposes</strong>
            <br><span class="g-color-primary">Don't miss</span> this
         </h2>
      </div>
      <p class="mb-0">Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem, vel faucibus ante. Nunc ullamcorper, justo a iaculis elementum, enim orci viverra eros, fringilla porttitor lorem eros vel odio.</p>
   </div>
   <div class="container">
      <!-- Product Blocks -->
      <div class="row">
         <div class="col-lg-6 g-mb-30">
            <!-- Article -->
            <a class="d-block u-bg-overlay g-bg-black-opacity-0_3--after g-parent g-text-underline--none--hover" href="#">
               <img class="img-fluid" src="./assets/img/img-temp/800x623/img1.jpg" alt="Image description">
               <!-- Article Content -->
               <div class="text-center text-uppercase u-bg-overlay__inner g-pos-abs g-bottom-10 g-left-10 g-max-width-200 ">
                  <div class="g-bg-primary-dark-v2 g-bg-primary-dark-v2-opacity-0_8--parent-hover g-py-20 g-px-30 g-transition-0_2 g-transition--ease-in">
                     <h3 class="h5 g-line-height-1_2 g-font-weight-700 g-font-size-16 g-font-secondary g-color-white g-mb-20">
                        <em class="d-block g-font-style-normal g-font-size-11 g-mb-10">Great Britain</em>
                        Big Big London
                     </h3>
                     <p class="g-line-height-1_6 g-font-size-10 g-color-white-opacity-0_8 mb-0">2 persons, 4 days, 5 nights, 4 stars hotel</p>
                  </div>
                  <div class="g-font-weight-700 g-font-size-18 g-color-white g-theme-bg-black-v1 g-pa-10">Only
                     <span class="g-color-primary">$400.00</span>
                  </div>
               </div>
               <!-- End Article Content -->
            </a>
            <!-- End Article -->
         </div>
         <div class="col-lg-6">
            <div class="row">
               <div class="col-sm-6 g-mb-30">
                  <!-- Article -->
                  <a class="d-block u-bg-overlay g-bg-black-opacity-0_3--after g-parent g-text-underline--none--hover" href="#">
                     <img class="img-fluid" src="./assets/img/img-temp/570x436/img1.jpg" alt="Image description">
                     <!-- Article Content -->
                     <div class="u-bg-overlay__inner g-pos-abs g-top-0 g-left-0 w-100 h-100 g-pa-10">
                        <div class="g-flex-middle h-100 g-bg-primary-dark-v2-opacity-0_8--parent-hover g-pa-10 g-transition-0_2 g-transition--ease-in">
                           <div class="text-center text-uppercase g-flex-middle-item--bottom">
                              <h3 class="h5 g-line-height-1_2 g-font-weight-700 g-font-size-16 g-font-secondary g-color-white g-mb-20">
                                 <em class="d-block g-font-style-normal g-font-size-11 g-mb-10">Greece</em>
                                 Heaven on Earth
                              </h3>
                              <strong class="d-inline-block g-line-height-1_2 g-font-weight-700 g-font-size-11 g-color-white g-bg-primary g-pa-10" href="#">$350.00</strong>
                           </div>
                        </div>
                     </div>
                     <!-- End Article Content -->
                  </a>
                  <!-- End Article -->
               </div>
               <div class="col-sm-6 g-mb-30">
                  <!-- Article -->
                  <a class="d-block u-bg-overlay g-bg-black-opacity-0_3--after g-parent g-text-underline--none--hover" href="#">
                     <img class="img-fluid" src="./assets/img/img-temp/570x436/img2.jpg" alt="Image description">
                     <!-- Article Content -->
                     <div class="u-bg-overlay__inner g-pos-abs g-top-0 g-left-0 w-100 h-100 g-pa-10">
                        <div class="g-flex-middle h-100 g-bg-primary-dark-v2-opacity-0_8--parent-hover g-pa-10 g-transition-0_2 g-transition--ease-in">
                           <div class="text-center text-uppercase g-flex-middle-item--bottom">
                              <h3 class="h5 g-line-height-1_2 g-font-weight-700 g-font-size-16 g-font-secondary g-color-white g-mb-20">
                                 <em class="d-block g-font-style-normal g-font-size-11 g-mb-10">Italy</em>
                                 Amazing Venice
                              </h3>
                              <strong class="d-inline-block g-line-height-1_2 g-font-weight-700 g-font-size-11 g-color-white g-bg-primary g-pa-10" href="#">$600.00</strong>
                           </div>
                        </div>
                     </div>
                     <!-- End Article Content -->
                  </a>
                  <!-- End Article -->
               </div>
               <div class="col-sm-6 g-mb-30">
                  <!-- Article -->
                  <a class="d-block u-bg-overlay g-bg-black-opacity-0_3--after g-parent g-text-underline--none--hover" href="#">
                     <img class="img-fluid" src="./assets/img/img-temp/570x436/img3.jpg" alt="Image description">
                     <!-- Article Content -->
                     <div class="u-bg-overlay__inner g-pos-abs g-top-0 g-left-0 w-100 h-100 g-pa-10">
                        <div class="g-flex-middle h-100 g-bg-primary-dark-v2-opacity-0_8--parent-hover g-pa-10 g-transition-0_2 g-transition--ease-in">
                           <div class="text-center text-uppercase g-flex-middle-item--bottom">
                              <h3 class="h5 g-line-height-1_2 g-font-weight-700 g-font-size-16 g-font-secondary g-color-white g-mb-20">
                                 <em class="d-block g-font-style-normal g-font-size-11 g-mb-10">France</em>
                                 Mysteriuos Paris
                              </h3>
                              <strong class="d-inline-block g-line-height-1_2 g-font-weight-700 g-font-size-11 g-color-white g-bg-primary g-pa-10" href="#">$350.00</strong>
                           </div>
                        </div>
                     </div>
                     <!-- End Article Content -->
                  </a>
                  <!-- End Article -->
               </div>
               <div class="col-sm-6 g-mb-30">
                  <!-- Article -->
                  <a class="d-block u-bg-overlay g-bg-black-opacity-0_3--after g-parent g-text-underline--none--hover" href="#">
                     <img class="img-fluid" src="./assets/img/img-temp/570x436/img4.jpg" alt="Image description">
                     <!-- Article Content -->
                     <div class="u-bg-overlay__inner g-pos-abs g-top-0 g-left-0 w-100 h-100 g-pa-10">
                        <div class="g-flex-middle h-100 g-bg-primary-dark-v2-opacity-0_8--parent-hover g-pa-10 g-transition-0_2 g-transition--ease-in">
                           <div class="text-center text-uppercase g-flex-middle-item--bottom">
                              <h3 class="h5 g-line-height-1_2 g-font-weight-700 g-font-size-16 g-font-secondary g-color-white g-mb-20">
                                 <em class="d-block g-font-style-normal g-font-size-11 g-mb-10">Malaysia</em>
                                 Langkawi, we love you
                              </h3>
                              <strong class="d-inline-block g-line-height-1_2 g-font-weight-700 g-font-size-11 g-color-white g-bg-primary g-pa-10" href="#">$800.00</strong>
                           </div>
                        </div>
                     </div>
                     <!-- End Article Content -->
                  </a>
                  <!-- End Article -->
               </div>
            </div>
         </div>
      </div>
      <!-- End Product Blocks -->
   </div>
</section>
<!-- End Purposes Section -->