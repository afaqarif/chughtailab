<section class="quote-port-cta">
  <div class="container">
    <div class="row align-items-center no-gutters">
      <div class="secondary-bg col-12 col-sm-6">
        <div class="cta-container p-3 p-lg-5">
          <div class="row align-items-center">
            <div class="col-2 offset-1">
              <a href="#" title="Portfolio">
                <img src="./assets/img/portfolio-icon.png">
              </a>
            </div>
            <div class="col-8 text-left">
              <h3>
                <a href="#" class="text-white" title="Find Medical Center">Find Medical Center
                </a>
              </h3>
              <p class="text-white mb-0">Take a look at what we’ve created
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="secondary-bg col-12 col-sm-6 active">
        <div class="cta-container p-3 p-lg-5">
          <div class="row align-items-center">
            <div class="col-2 offset-1">
              <a href="#" title="Request Quote">
                <img src="./assets/img/quote-icon.png">
              </a>
            </div>
            <div class="col-8 text-left">
              <h3>
                <a href="#" class="text-white" title="Book an Appointment">Book an Appointment 
                </a>
              </h3>
              <p class="text-white mb-0">Let's work on your health
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
