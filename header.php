<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Title -->
      <title>Chughtai</title>
      <!-- Required Meta Tags Always Come First -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <!-- Favicon -->
      <link rel="shortcut icon" href="./assets/img/favicon-chughtailab.ico">
      <!-- Google Fonts -->
      <!-- <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&family=Roboto:wght@400;700&display=swap" rel="stylesheet"> -->
      <!-- CSS Global Compulsory -->
      <!-- <link rel="stylesheet" href="./assets/css/bootstrap.min.css"> -->
      <!-- CSS Implementing Plugins -->
      <!-- <link rel="stylesheet" href="./assets/css/font-awesome.min.css">
      <link rel="stylesheet" href="./assets/css/masterslider.main.css">
      <link rel="stylesheet" href="./assets/css/jquery-ui.min.css">
      <link rel="stylesheet" href="./assets/css/animate.css">
      <link rel="stylesheet" href="./assets/css/slick.css"> -->

      <!-- <link rel="stylesheet" href="./assets/css/simple-line-icons.css"> -->

      <!-- <link rel="stylesheet" href="./assets/css/style.css"> -->
      
      <!-- <link rel="stylesheet" href="./assets/css/chosen.css"> -->
      <!-- <link rel="stylesheet" href="./assets/css/hamburgers.min.css"> -->

      
      
      
      <!-- <link rel="stylesheet" href="./assets/css/cubeportfolio.min.css"> -->
      <!-- <link rel="stylesheet" href="./assets/css/magnific-popup.css"> -->
      
      <!-- CSS Template -->
      
      <!-- CSS Customization -->
      <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="./assets/css/libaries.css">
      <!-- <link rel="stylesheet" type="text/css" href="./assets/css/chosen.css"> -->
      <link rel="stylesheet" href="./assets/css/styles.op-travel.css">
      <link rel="stylesheet" href="./assets/css/custom.css">
      <link rel="stylesheet" type="text/css" href="./assets/css/main.css">
   </head>
   <body>
     <!--  Start Main Section -->
      <main>
            <!-- Start Navigation Section -->
            <?php include 'inc/navigation_section.php'; ?>